<?php 
  //Scan the directoru and print out the ones we find. 
  // (nothing special going on here)
  $dirtoscan = getcwd();
  $directorylistings = scandir($dirtoscan);
  $dirs = "";
  foreach ($directorylistings as $directory) {
    $dirinfo = pathinfo($directory);
    $stats = lstat($directory);
    if (is_dir("$directory") && $dirinfo['filename'] && $dirinfo['filename'] != '.' && $directory != "cgi-bin" && $directory != "di") {
      //Using bootstrap's nifty "thumbnail feature" with a dummy image generator
      $dirs .= "<li><i class=\"gen-enclosed foundicon-website\"><p><a href='$directory'>$directory</a></p></i></li>";      
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Colin's Sites:</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le styles -->
    <link href="foundation/stylesheets/foundation.css" rel="stylesheet">
    <link rel="stylesheet" href="general_enclosed_foundicons.css">
    <style type="text/css">
      body {
        padding-bottom: 40px;
      }
      ul li { list-style: none; font-size: 26px; overflow: hidden; height: 100%; }
      ul li p { font-size: 20px; display: inline; position: relative; top: -6px; margin-left: 8px; font-style: normal; }
      .button { margin-right: 1em; }
      header { background-color: #2ba6cb; padding: 22px 0; }
			header h1 { color: #fff; font-weight: 500; }
			header h4 { color: #fff; font-weight: 300; }
			#mainContent { margin-top: 44px; margin-bottom: 22px; padding-bottom: 22px; border-bottom: 1px solid #eee; overflow: hidden; }

    </style>
  </head>

  <body>
	  <header>
	    <div class="row">
	      <div class="twelve columns">
	        <h1>Colin's Sites</h1>
	        <h4>Choose from one of the sites below!</h4>
	      </div>
	    </div>
	  </header>
	  
	  <section id="mainContent">
        <div class="row">
          <ul class="block-grid three-up">
                <?php print $dirs;?>        
          </ul>
        </div>  
  	</section>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  <script src="foundation/javascripts/foundation.min.js"></script>


  </body>
</html>

